import React, {Component} from 'react';
import './App.css';
import Login from "./screens/Login";
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom'
import Register from "./screens/Register";
import MainView from "./screens/MainView";
import Config from "./Config"

class App extends Component {

    render() {
        const PrivateRoute = ({component: Component, ...rest}) => (
            <Route {...rest} render={(props) => (
                Config.getZeton() != null
                    ? <Component {...props} />
                    : <Redirect to='/login'/>
            )}/>);

        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/register" component={Register}/>
                    <Route path="/login" component={Login}/>
                    <PrivateRoute path="/app" component={MainView}/>
                    <Redirect to="/login"/>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
