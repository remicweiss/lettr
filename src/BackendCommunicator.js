import Config from "./Config";
import queryString from 'query-string'

export default class BackendCommunicator {
    static URL = ".netlify/functions/server/";

    static isUsernameAvailable = async (username) => {
        return await BackendCommunicator.fetchGET("free_username", {query: username}, false)
    };

    static register = async (registerForm) => {
        let resp = await BackendCommunicator.fetchPOST("register", registerForm, false);
        return await resp;
    };

    static addFriendship = async (friendid) => {
        return await BackendCommunicator.fetchPOST("add_friendship", {
            user_id: Config.getUser().IdUporabnika,
            friend_id: friendid
        }, true);
    };

    static login = async (loginForm) => {
        return await BackendCommunicator.fetchPOST("login", loginForm, false);
    };

    static getUser = async () => {
        return await BackendCommunicator.fetchGET("get_user", {}, true);
    };

    static getMessages = async (friendshipId) => {
        let resp = await BackendCommunicator.fetchGET("get_messages", {
            user_id: Config.getUser().IdUporabnika,
            friendship_id: friendshipId,
            delta: false
        }, true);
        return resp.messages;
    };

    static getMessagesDelta = async (friendshipId) => {
        let resp = await BackendCommunicator.fetchGET("get_messages", {
            user_id: Config.getUser().IdUporabnika,
            friendship_id: friendshipId,
            delta: true
        }, true);
        return resp.messages;
    };

    static sendMessage = async (friendshipId, content) => {
        await BackendCommunicator.fetchPOST("send_message", {
            user_id: Config.getUser().IdUporabnika,
            friendship_id: friendshipId,
            content: content
        }, true);
    };

    static getPossibleFriends = async (query) => {
        let resp = await BackendCommunicator.fetchGET("friendship_search", {query: query}, true);
        return resp.possibleFriends;
    };

    static getFriends = async () => {
        let resp = await BackendCommunicator.fetchGET("friendships", {user_id: Config.getUser().IdUporabnika}, true);
        return resp.friendships;
    };

    static fetchPOST = async (endpoint, body, needsAuth) => {
        let headers = new Headers({'Content-Type': 'application/json'});
        if (needsAuth) {
            headers.append('Authorization', "Basic " + Config.getZeton());
        }
        let response = await fetch(BackendCommunicator.URL + endpoint, {
            method: "POST",
            body: JSON.stringify(body),
            headers: headers
        });
        return await response.json();
    };

    static fetchGET = async (endpoint, params, needsAuth) => {
        let headers = new Headers();
        if (needsAuth) {
            headers.append('Authorization', "Basic " + Config.getZeton());
        }

        let response = await fetch(BackendCommunicator.URL + endpoint + "?" + queryString.stringify(params), {
            method: "GET",
            headers: headers
        });
        return response.json();
    };
}