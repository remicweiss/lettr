import BackendCommunicator from "./BackendCommunicator";

export default class Config {

    static user = null;

    static getUser = () => {
        return Config.user
    };

    static saveUser = (userObject) => {
        Config.user = userObject;
    };

    static getZeton = () => {
        try {
            return localStorage.getItem('token');
        } catch (e) {
            return null;
        }
    };

    static saveZeton = async (zeton) => {
        localStorage.setItem('token', zeton);
        let ok = await Config.retrieveUser();
        if(!ok) {
            Config.logout();
        }
    };

    static retrieveUser = async () => {
        let userResponse = await BackendCommunicator.getUser();
        let user = userResponse.user;
        if(userResponse.status!=="LOGIN_OK")
        {
            return false;
        }
        Config.saveUser(user);
        return true;
    };
    static getChat = () => {
        try {
            return JSON.parse(localStorage.getItem('chat'));
        } catch (e) {
            return null;
        }
    };

    static setChat = (chatObject) => {
        localStorage.setItem('chat', JSON.stringify(chatObject));
    };

    static logout = () => {
        Config.user = null;
        localStorage.removeItem('token');
        localStorage.removeItem('chat');

    }
}