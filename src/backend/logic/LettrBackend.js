const image2base64 = require('image-to-base64');

class LettrBackend {

    constructor() {
        this.mysql = require('serverless-mysql/index')({
            config: {
                host: "jeranciceva.prw.si",
                port: 3316,
                user: "lettr_backend",
                password: "Lettrdev1.",
                database: "lettr_prod_2",
                charset: "utf8mb4"
            }
        });
    }

    connect = async () => {
        return await this.mysql.connect();
    };

    login = async (username, password) => {
        try {
            let getUserResponse = await this.mysql.query("CALL spPridobiUporabnika(?,?)", [username, password]);
            await this.mysql.end();
            if (getUserResponse[0].length > 0)
                return {
                    status: "LOGIN_OK",
                    user: getUserResponse[0][0]
                }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Napačno uporabniško ime ali geslo, ali pa je uporabnik deaktiviran.",
            user: {}
        }
    };

    register = async (username, password, fullname, avatarbase64) => {

        try {
            //Get custom made avatar :)
            if (avatarbase64 === "") {
                avatarbase64 = "data:image/png;base64," + await image2base64("https://ui-avatars.com/api/?name=" + username);
            }
            let createUserResponse = await this.mysql.query("CALL spUstvariUporabnika(?,?,?,?)", [username, password, fullname, avatarbase64]);
            await this.mysql.end();
            return {
                status: "REGISTER_OK",
                user: createUserResponse[0][0]
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Napaka na strežniku.",
            user: {}
        }
    };

    addFriendship = async (userid, friendid) => {

        try {
            let addFriendResponse = await this.mysql.query("CALL spNovoPrijateljstvo(?,?)", [userid, friendid]);
            await this.mysql.end();
            return {
                status: "NEW_FRIENDSHIP_OK",
                friendship: addFriendResponse[0][0]
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Napaka pri ustvarjanju prijateljstva",
            friendship: {}
        }
    };

    getFriendships = async (userid) => {

        try {
            let addFriendResponse = await this.mysql.query("CALL spPridobiPrijateljeUporabnika(?)", [userid]);
            await this.mysql.end();
            return {
                status: "GET_FRIENDSHIP_OK",
                friendships: addFriendResponse[0]
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Napaka pri pridobivanju prijateljstva",
            friendships: []
        }
    };

    isUsernameAvailable = async (username) => {

        try {
            let usernameAvailableResponse = await this.mysql.query("SELECT fnJeUporabniskoImeProsto(?) AS UporabniskoImeProsto", [username]);
            await this.mysql.end();
            return {
                status: "USERNAME_CHECK_OK",
                username_free: usernameAvailableResponse[0].UporabniskoImeProsto === 1
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Napaka pri pridobivanju prostega uporabniškega imena",
            username_free: false
        }
    };

    loginWithToken = async (token) => {
        try {
            let getUserResponse = await this.mysql.query("CALL spPridobiUporabnikaPrekoAvtorizacije(?)", [token.replace("Basic ", "")]);
            await this.mysql.end();
            if (getUserResponse[0].length > 0)
                return {
                    status: "LOGIN_OK",
                    user: getUserResponse[0][0]
                }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Napačno uporabniško ime ali geslo, ali pa je uporabnik deaktiviran.",
            user: {}
        }
    };

    authorize = async (token) => {
        try {
            let authorizationResponse = await this.mysql.query("SELECT fnAvtorizacijaZahtevka(?) AS Authorized", [token.replace("Basic ", "")]);
            await this.mysql.end();
            return {
                status: "AUTH_CKECK_OK",
                authorized: authorizationResponse[0].Authorized === 1
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Napaka pri pridobivanju avtorizacije",
            authorized: false
        }
    };

    userSearch = async (username) => {
        try {
            let getUserResponse = await this.mysql.query("CALL spPridobiMoznePrijatelje(?)", [username]);
            await this.mysql.end();
            return {
                status: "FREN_SEARCH_OK",
                possibleFriends: getUserResponse[0]
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Pridobivanje nedodanih prijateljev je spodletelo",
            possibleFriends: []
        }
    };

    sendMessage = async (userId, friendId, content) => {
        try {
            let sendResponse = await this.mysql.query("SELECT fnPosljiSporociloUporabniku(?,?,?)", [userId, friendId, content]);
            await this.mysql.end();
            return {
                status: "SEND_OK",
                sendMessageResponse: sendResponse
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Pošiljanje sporočila spodletelo",
            sendMessageResponse: {}
        }
    };

    getMessages = async (user_id, prijateljstvoid, delta) => {
        try {
            let getMessagesResponse = await this.mysql.query("CALL spPridobiSporocila(?,?,?)", [user_id, prijateljstvoid, delta === "true"]);
            await this.mysql.end();
            return {
                status: "MSG_GET_OK",
                messages: getMessagesResponse[0]
            }
        } catch (e) {
            console.error(e);
        }
        return {
            status: "Pridobivanje sporočil spodletelo.",
            messages: []
        }
    };
}

module.exports = LettrBackend;
