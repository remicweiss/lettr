const LettrBackend = require("./logic/LettrBackend");
const express = require('express');
const serverless = require('serverless-http');

const app = express();
const bodyParser = require('body-parser');

const router = express.Router();
let backend = new LettrBackend();

const checkAuth = async (req, res, next) => {
    if ((await backend.authorize(req.headers.authorization)).authorized) {
        next();
    } else {
        return res.status(403).json({
            status: 403,
            message: 'FORBIDDEN'
        })
    }
};

router.get('/ping', checkAuth, (req, res) => {
    res.send("pong");
});

router.post('/login', async (req, res) => {
    await backend.connect();
    res.send(await backend.login(req.body.username, req.body.password));
});

router.get('/get_user', checkAuth, async (req, res) => {
    await backend.connect();
    res.send(await backend.loginWithToken(req.headers.authorization));
});

router.get('/friendships', checkAuth, async (req, res) => {
    await backend.connect();
    res.send(await backend.getFriendships(req.query.user_id));
});

router.get('/friendship_search', checkAuth, async (req, res) => {
    await backend.connect();
    res.send(await backend.userSearch(req.query.query));
});

router.get('/get_messages', checkAuth, async (req, res) => {
    await backend.connect();
    res.send(await backend.getMessages(req.query.user_id, req.query.friendship_id, req.query.delta));
});

router.post('/send_message', checkAuth, async (req, res) => {
    await backend.connect();
    res.send(await backend.sendMessage(req.body.user_id, req.body.friendship_id, req.body.content));
});

router.post('/add_friendship', checkAuth, async (req, res) => {
    await backend.connect();
    res.send(await backend.addFriendship(req.body.user_id, req.body.friend_id));
});

router.get('/free_username', async (req, res) => {
    await backend.connect();
    res.send(await backend.isUsernameAvailable(req.query.query));
});

router.post('/register', async (req, res) => {
    await backend.connect();
    res.send(await backend.register(req.body.username, req.body.password, req.body.name, req.body.avatar));
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/.netlify/functions/server', router);

module.exports = app;
module.exports.handler = serverless(app);
exports.handler = serverless(app);