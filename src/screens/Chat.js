import React, {Component} from 'react';
import Config from "../Config";
import {Comment, Input, Loader} from "semantic-ui-react";
import BackendCommunicator from "../BackendCommunicator";

export default class Chat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            friend: null,
            messages: null,
            content: ""
        }
    }

    interval;

    async componentDidMount() {
        if (this.state.friend !== this.props.friend) {
            let messages = await BackendCommunicator.getMessages(this.props.friend.PrijateljstvoId);
            this.setState({friend: this.props.friend, messages: messages});
        }
        this.pollMessages()
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    async componentWillReceiveProps(nextProps, nextContext) {
        if (this.state.friend !== nextProps.friend) {
            let messages = await BackendCommunicator.getMessages(nextProps.friend.PrijateljstvoId);
            this.setState({friend: nextProps.friend, messages: messages});
        }
    }

    handleSend = async () => {
        this.setState({loadingSend: true});
        await BackendCommunicator.sendMessage(this.state.friend.PrijateljstvoId,this.state.content);
        this.setState({
            loadingSend: false, messages: [{
                "id": "local",
                "PosiljateljId": Config.getUser().IdUporabnika,
                "PrijateljstvoId": this.state.friend.PrijateljstvoId,
                "Vsebina": this.state.content
            }, ...this.state.messages]
        });
    };

    pollMessages = () => {
        this.interval = setInterval(async () => {

            this.setState({
                messages: [...await BackendCommunicator.getMessagesDelta(this.state.friend.PrijateljstvoId), ...this.state.messages]
            })
        }, 2000);
    };

    handleKeyDown = async (e) => {
        if (this.state.content !== "" && !this.state.loadingSend) {
            clearInterval(this.interval);
            if (e.key === 'Enter') {
                await this.handleSend();
                this.setState({content: ""})
            }
            this.pollMessages();
        }
    };

    render() {
        if (this.state.messages !== null) {
            return (
                <div>
                    <Input loading={this.state.loadingSend} value={this.state.content} onChange={(e, r) => {
                        this.setState({content: r.value})
                    }} placeholder='Napiši kaj!' fluid onKeyDown={this.handleKeyDown} autoFocus/>
                    <Comment.Group threaded>
                        {this.state.messages.map((message, i) => {
                            if (message.PosiljateljId !== Config.getUser().IdUporabnika) {
                                return (
                                    <Comment key={i}>
                                        <Comment.Avatar src={this.state.friend.Slika}/>
                                        <Comment.Content>
                                            <Comment.Author>{this.state.friend.PolnoIme}</Comment.Author>
                                            <Comment.Text>{message.Vsebina}</Comment.Text>
                                        </Comment.Content>
                                    </Comment>)
                            } else {
                                return (
                                    <Comment key={i}>
                                        <Comment.Avatar src={Config.getUser().Slika}/>
                                        <Comment.Content>
                                            <Comment.Author>{Config.getUser().PolnoIme}</Comment.Author>
                                            <Comment.Text>{message.Vsebina}</Comment.Text>
                                        </Comment.Content>
                                    </Comment>)
                            }
                        })}
                    </Comment.Group>
                </div>
            );
        } else {
            return (
                <div>
                    <Loader active inline='centered'>Nalagam sporočila</Loader>
                </div>
            );
        }
    }
}