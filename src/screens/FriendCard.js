import React, {Component} from 'react';
import {Card, Image} from "semantic-ui-react";

export default class FriendCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            friend: this.props.friend
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (this.state.friend !== nextProps.friend) {
            this.setState({friend: nextProps.friend});
        }
    }

    render() {
        return (
            <Card onClick={() => {
                this.props.setChattingFriend(this.state.friend);
            }}>
                <Image src={this.state.friend.Slika} wrapped ui={false}/>
                <Card.Content>
                    <Card.Header>{this.state.friend.PolnoIme}</Card.Header>
                    <Card.Meta>{this.state.friend.UporabniskoIme}</Card.Meta>
                </Card.Content>
            </Card>
        );
    }
}