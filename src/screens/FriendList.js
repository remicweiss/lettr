import React, {Component} from 'react';
import {Card, Input, Loader} from "semantic-ui-react";
import Config from "../Config";
import FriendCard from "./FriendCard";
import NewFriendCard from "./NewFriendCard";
import Chat from "./Chat";
import BackendCommunicator from "../BackendCommunicator";

export default class FriendList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            friendships: [],
            friendshipsFiltered: [],
            loading: false,
            loadingText: false,
            searchStr: "",
            selectedFriend: null
        }
    }

    setChattingFriend = async (friend) => {
        Config.setChat(friend);
        this.setState({
            selectedFriend: friend
        })
    };

    async componentDidMount() {

        await this.retrieveFriends();

        window.onpopstate = (e) => {
            Config.setChat(null);
        }
    }

    retrieveFriends = async () => {

        this.setState({loading: true});
        let friendships_aux = await BackendCommunicator.getFriends();

        this.setState({loading: false});

        this.setState({
            friendships: friendships_aux,
            friendshipsFiltered: friendships_aux,
            searchStr: "",
            selectedFriend: Config.getChat()
        });
    };

    userFieldSearch = async (e, r) => {
        this.setState({loadingText: true, searchStr: r.value});
        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = setTimeout(async () => {
            await this.searchUsers();
        }, 300);
    };

    searchUsers = async () => {
        let friendshipsFiltered = this.state.friendships.filter((friendship) => {
            return friendship.UporabniskoIme.toLowerCase().includes(this.state.searchStr.toLowerCase()) || friendship.PolnoIme.toLowerCase().includes(this.state.searchStr.toLowerCase())
        });

        if (friendshipsFiltered.length === 0) {
            friendshipsFiltered.push(...(await BackendCommunicator.getPossibleFriends(this.state.searchStr)).filter((prijatelj)=>{
                return prijatelj.id!==Config.getUser().IdUporabnika && prijatelj.id!==undefined;
            }));
        }

        this.setState({
            friendshipsFiltered: friendshipsFiltered
        });
        this.setState({loadingText: false});
    };

    render() {
        if (this.state.selectedFriend !== null) {
            return (
                <Chat friend={this.state.selectedFriend}/>
            )
        }

        if (!this.state.loading) {
            return (
                <div>
                    <Input loading={this.state.loadingText} fluid icon={{name: 'search'}}
                           onChange={this.userFieldSearch} placeholder='Išči prijatelje'
                           style={{marginBottom: "10px"}}/>
                        <Card.Group centered>
                            {this.state.friendshipsFiltered.map((friend) => {
                                if (friend.id === undefined) {
                                    return (
                                        <FriendCard setChattingFriend={this.setChattingFriend} key={friend.PrijateljId}
                                                    friend={friend}/>
                                    );
                                } else {
                                    return (
                                        <NewFriendCard key={friend.id} friend={friend}
                                                       newFriendAddedCallback={this.retrieveFriends}/>
                                    );
                                }
                            })}
                        </Card.Group>
                </div>
            );
        } else {
            return (
                <div>
                    <Loader active inline='centered'>Nalagam prijatelje</Loader>
                </div>
            );
        }

    }
}