import React, {Component} from 'react';
import {Button, Form, Header, Icon, Image, Message, Modal} from "semantic-ui-react";
import Config from "../Config";
import BackendCommunicator from "../BackendCommunicator";

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loginForm: {
                username: "",
                password: ""
            },
            loading: false,
            loginError: ""
        };
    }

    componentDidMount() {
        if (Config.getZeton() !== null) {
            this.props.history.push('/app');
        }
    }

    editForm = (e, r) => {
        let loginForm = this.state.loginForm;
        loginForm[r.name] = r.value;
        this.setState({loginForm: loginForm});
    };

    doLogin = async () => {
        try {
            this.setState({loading: true});

            let data = await BackendCommunicator.login(this.state.loginForm);

            if (data.status === "LOGIN_OK") {
                await Config.logout();
                await Config.saveZeton(data.user.Zeton);
                this.props.history.push('/app');
                return;
            } else {
                this.setState({loginError: data.status})
            }
            
            this.setState({loading: false});
        } catch (e) {
            console.error(e);
        }
    };

    render() {
        return (
            <div>
                <Modal basic dimmer={"inverted"} open={true} size={"tiny"}>
                    <Modal.Content image>
                        <Image wrapped fluid centered src={require('../images/logo_transparent.png')}/>
                        <Modal.Description style={{width: "100%"}}>
                            <Header>Vpis</Header>
                            <Form>
                                <Form.Field>
                                    <label>Uporabniško ime</label>
                                    <Form.Input onChange={this.editForm} name={"username"}
                                                placeholder='Uporabniško ime'  autoComplete="username"/>
                                </Form.Field>
                                <Form.Field>
                                    <label>Geslo</label>
                                    <Form.Input name={"password"} onChange={this.editForm} autoComplete="current-password" type='password'
                                                placeholder='Geslo'/>
                                </Form.Field>
                                <Message negative hidden={this.state.loginError.length === 0}>
                                    <Message.Header>Napaka pri prijavi</Message.Header>
                                    <p>
                                        {this.state.loginError}
                                    </p>
                                </Message>
                                <Button animated={"fade"} loading={this.state.loading} color={"black"} type='submit'
                                        onClick={this.doLogin} fluid>
                                    <Button.Content visible>Vpis</Button.Content>
                                    <Button.Content hidden>
                                        <Icon name='sign-in'/>
                                    </Button.Content>
                                </Button>
                                <br/>
                                <Button fluid basic color={"grey"} onClick={() => {
                                    this.props.history.push("/register")
                                }}>Registracija</Button>
                            </Form>
                        </Modal.Description>
                    </Modal.Content>
                </Modal>
            </div>
        );
    }
}