import React, {Component} from 'react';
import Config from "../Config";
import {Dropdown, Image, Loader, Menu} from "semantic-ui-react";
import FriendList from "./FriendList";

export default class MainView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    async componentWillMount() {
        if (Config.getUser() === null) {
            let ok = await Config.retrieveUser();
            if(!ok)
            {
                Config.logout();
                this.props.history.push('/');
                return;
            }
        }
        this.setState({
            loading: false
        })
    }

    render() {
        if (!this.state.loading) {
            return (
                <div>
                    <Menu pointing secondary>
                        <Menu.Menu position='left'>
                            <Menu.Item style={{height: "100%"}} header onClick={() => {
                                Config.setChat(null);
                                this.props.history.push('/');

                            }}>Lettrchat</Menu.Item>
                        </Menu.Menu>
                        <Menu.Menu position='right' style={{marginRight: "3px"}}>
                            <Menu.Item style={{height: "100%", paddingRight: "5px"}}>
                                <Image avatar src={Config.getUser().Slika}/>
                            </Menu.Item>
                            <Dropdown item style={{height: "100%", paddingLeft: "0px"}}
                                      text={Config.getUser().PolnoIme}>
                                <Dropdown.Menu>
                                    <Dropdown.Item icon='sign-out' text='Odjava' onClick={() => {
                                        Config.logout();
                                        this.props.history.push('/login');
                                    }}/>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu.Menu>
                    </Menu>
                    <div style={{margin: "20px"}}>
                        <FriendList setChattingFriend={this.setChattingFriend}/>
                    </div>
                </div>
            );
        } else {
            return (
                <div>
                    <Loader active inline='centered' style={{top: "20px"}}>Nalagam aplikacijo</Loader>
                </div>)
        }
    }
}