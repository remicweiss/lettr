import React, {Component} from 'react';
import {Button, Card, Icon, Image} from "semantic-ui-react";
import BackendCommunicator from "../BackendCommunicator";

export default class NewFriendCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            friend: this.props.friend,
            loading: false
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (this.state.friend !== nextProps.friend) {
            this.setState({friend: nextProps.friend});
        }
    }

    addFriend = async () => {
        this.setState({loading: true});
        await BackendCommunicator.addFriendship(this.state.friend.id);
        this.setState({loading: false});
        this.props.newFriendAddedCallback();
    };

    render() {
        return (
            <Card>
                <Image src={this.state.friend.Slika} wrapped ui={false}/>
                <Card.Content>
                    <Card.Header>{this.state.friend.PolnoIme}</Card.Header>
                    <Card.Meta>{this.state.friend.UporabniskoIme}</Card.Meta>
                </Card.Content>
                <Card.Content extra>
                    <Button animated='fade' basic color={"green"} fluid loading={this.state.loading}
                            onClick={this.addFriend}>
                        <Button.Content visible>Dodaj</Button.Content>
                        <Button.Content hidden><Icon name='plus'/></Button.Content>
                    </Button>
                </Card.Content>
            </Card>
        );
    }
}