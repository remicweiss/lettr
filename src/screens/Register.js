import React, {Component} from 'react';
import {Form, Header, Icon, Image, Message, Modal} from "semantic-ui-react";
import Config from '../Config'
import BackendCommunicator from "../BackendCommunicator";

const username_requirements = [
    'Črke male/velike a-z',
    'Številke',
];

export default class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            registerForm: {
                username: "",
                password: "",
                name: "",
                avatar: "",
                recaptcha: ""
            },
            usernameOk: true,
            usernameOKLoading: false,
            loading: false,
            registerError: ""
        };
    }

    componentDidMount() {
        if (Config.getUser() !== null) {
            this.props.history.push('/app');
        }
    }

    editForm = async (e, r) => {
        let registerForm = this.state.registerForm;
        registerForm[r.name] = r.value;
        return new Promise((resolve) => {
            this.setState({registerForm: registerForm}, resolve);
        });
    };

    editUsername = async (e, r) => {
        this.setState({usernameOKLoading: true});
        let searchText = r.value;
        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = setTimeout(async () => {
            let usernameAvailableReq = await BackendCommunicator.isUsernameAvailable(searchText);
            if (usernameAvailableReq.username_free) {
                this.editForm(e, r);
                this.setState({usernameOk: true})
            } else {
                r["value"] = "";
                this.editForm(e, r);
                this.setState({usernameOk: false})
            }
            this.setState({usernameOKLoading: false});
        }, 300);
    };

    isUserEligebleForReg = () => {
        return (this.state.registerForm.username === "" ||
            this.state.registerForm.password === "" ||
            this.state.registerForm.name === "" ||
            this.state.registerForm.username.length > 20 ||
            this.state.registerForm.password.length > 20 ||
            this.state.registerForm.name.length > 20)
    };

    doRegister = async () => {
        this.setState({loading: true});

        let data = await BackendCommunicator.register(this.state.registerForm);
        if (data.status === "REGISTER_OK") {
            this.props.history.push('/login');
            return;
        } else {
            this.setState({registerError: data.status})
        }

        this.setState({loading: false});

    };

    render() {
        return (
            <div>
                <Modal basic dimmer={"inverted"} open={true}>
                    <Modal.Content image>
                        <Image wrapped fluid centered src={require('../images/logo_transparent.png')}/>
                        <Modal.Description style={{width: "100%"}}>
                            <Header>Registracija</Header>
                            <Form>
                                <Form.Field>
                                    <label>Ime</label>
                                    <Form.Input
                                        name={"name"}
                                        placeholder='Polno ime' onChange={this.editForm}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>Uporabniško ime</label>
                                    <Form.Input
                                        loading={this.state.usernameOKLoading}
                                        onKeyPress={(event) => {
                                            const isLetter = /^[a-z]$/i.test(event.key);
                                            const isNumber = /^[0-9]$/i.test(event.key);

                                            if (!isLetter && !isNumber) {
                                                event.preventDefault();
                                            }
                                        }}
                                        onChange={this.editUsername}
                                        error={!this.state.usernameOk}
                                        name={"username"}
                                        icon={<Icon name={this.state.usernameOk ? 'check' : 'close'}
                                                    color={this.state.usernameOk ? 'green' : 'red'}/>
                                        }
                                        placeholder='Uporabniško ime'>
                                    </Form.Input>
                                </Form.Field>
                                <Message>
                                    <Message.Header>Zahteve uporabniškega imena</Message.Header>
                                    <Message.List items={username_requirements}/>
                                </Message>
                                <Form.Field>
                                    <label>Geslo</label>
                                    <Form.Input
                                        name={"password"}
                                        type='password'
                                        placeholder='Geslo' onChange={this.editForm}/>
                                </Form.Field>
                                <Message negative hidden={this.state.registerError.length === 0}>
                                    <Message.Header>Napaka v registraciji</Message.Header>
                                    <p>
                                        {this.state.registerError}
                                    </p>
                                </Message>
                                <Form.Button loading={this.state.loading} disabled={this.isUserEligebleForReg()}
                                             color={"black"} fluid type='submit' onClick={this.doRegister}>Registriraj
                                    se!</Form.Button>
                            </Form>
                        </Modal.Description>
                    </Modal.Content>
                </Modal>
            </div>
        );
    }
}